<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// Welcome message
$router->get('/', function () use ($router) {
    return response()->json(['welcome' => 'Chat Server']);
});

// Endpoints with authentication required
$router->group(['middleware' => 'auth'], function () use ($router) {
    // Get authenticated user info
    $router->get('/me', 'UserController@me');
    // Get user by login
    $router->get('/user/{login}', 'UserController@aboutUser');
    // Get user dialogs
    $router->get('/dialogs/list', 'UserController@dialogs');
    // Add dialog
    $router->get('/dialogs/{login}/add', 'UserController@addDialog');
    // Get the dialog history
    $router->get('/dialogs/{login}/history', 'UserController@history');
    // Send message to the dialog
    $router->post('/dialogs/{login}/message', 'UserController@message');
});

// Sign up
$router->post('/signup', 'UserController@signUp');
// Sign in
$router->post('/signin', 'UserController@signIn');