<?php

namespace App\Http\Controllers;

use App\Dialog;
use App\Message;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class UserController extends Controller
{
    // Get authenticated user info
    public function me(Request $request)
    {
        return response()->json($request->user());
    }

    // Get user by login
    public function aboutUser($login)
    {
        $user = User::where('login', $login)->first();
        return response()->json($user);
    }

    // Get user dialogs
    public function dialogs(Request $request)
    {
        $user_id = $request->user()->id;
        $dialogs = $request->user()->dialogs->map(function ($dialog) use ($user_id) {
            return [
                'created_at' => $dialog->created_at,
                'user' => $dialog->users()->where('user_id', '<>', $user_id)->first()
            ];
        });
        return response()->json(['dialogs' => $dialogs]);
    }

    // Add new dialog with user
    public function addDialog($login, Request $request)
    {
        $user = $request->user();
        $partner = User::where('login', $login)->first();
        // Check if user exist
        if (empty($partner)) {
            return response()->json(['success' => false, 'error' => 'User not found']);
        }
        // Check if dialog already exist
        $dialog = $user->dialogWith($partner);
        if (empty($dialog)) { // Create new dialog if not exist
            /** @var Dialog $dialog */
            $dialog = Dialog::create();
            $dialog->users()->attach(new Collection([$user, $partner]));
        }
        return response()->json(['success' => true]);
    }

    // Get the dialog history
    public function history($login, Request $request)
    {
        $user = $request->user();
        $partner = User::where('login', $login)->first();
        // Check if user exist
        if (empty($partner)) {
            return response()->json(['success' => false, 'error' => 'User not found']);
        }
        $dialog_id = $user->dialogWith($partner);
        if (empty($dialog_id)) {
            return response()->json(['success' => false, 'error' => 'Dialog not exist']);
        } else {
            $dialog = Dialog::find($dialog_id[0]->dialog_id);
            $messages = [];
            if (!empty($dialog)) {
                $messages = $dialog->messages;
            }
            return response()->json(['success' => true, 'messages' => $messages]);
        }
    }

    // Send message to the dialog
    public function message($login, Request $request)
    {
        $user = $request->user();
        $partner = User::where('login', $login)->first();
        // Check if user exist
        if (empty($partner)) {
            return response()->json(['success' => false, 'error' => 'User not found']);
        }
        $dialog_id = $user->dialogWith($partner);
        if (empty($dialog_id)) {
            return response()->json(['success' => false, 'error' => 'Dialog not exist']);
        } else if (
            !empty($dialog = Dialog::find($dialog_id[0]->dialog_id)) &&
            !empty($request->input('content'))
        ) {
            $message = new Message();
            $message->sender()->associate($user);
            $message->dialog()->associate($dialog);
            $message->content = $request->input('content');
            $message->save();
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }

    // Sign up
    public function signUp(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'login' => 'required|unique:users',
            'password' => 'required',
            'public_key' => 'required'
        ]);

        $user = new User();
        $user->login = $request->input('login');
        $user->name = $request->input('name');
        $user->password = app('hash')->make($request->input('password'));
        $user->api_token = Uuid::uuid4()->toString();
        $user->public_key = $request->input('public_key');
        $user->save();

        return response()->json(['success' => true, 'api_token' => $user->api_token]);
    }

    // Sign in
    public function signIn(Request $request)
    {
        $this->validate($request, [
            'login' => 'required',
            'password' => 'required'
        ]);

        if (!empty($user = User::where('login', $request->input('login'))->first())) {
            if (app('hash')->check($request->input('password'), $user->password)) {
                return response()->json(['api_token' => $user->api_token]);
            }
        }

        return response()->json(['success' => false, 'error' => 'Invalid credentials']);
    }
}
