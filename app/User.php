<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'login', 'public_key'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'api_token'
    ];

    public function getAuthIdentifierName()
    {
        return 'login';
    }

    public function dialogs()
    {
        return $this->belongsToMany(Dialog::class);
    }

    public function dialogWith(User $partner)
    {
        return app('db')->select('
select d1.dialog_id
        from (dialog_user d1 
             inner join dialog_user d2 on d1.dialog_id = d2.dialog_id 
             and d1.user_id <> d2.user_id and d1.user_id = ? and d2.user_id = ?);',
            [
                $this->id,
                $partner->id
            ]);
    }
}
